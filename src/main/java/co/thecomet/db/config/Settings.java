package co.thecomet.db.config;

import co.thecomet.db.config.components.Database;
import co.thecomet.common.config.JsonConfig;

public class Settings extends JsonConfig {

    private Database database = new Database();

    public Database getDatabase() {
        return database;
    }
}
