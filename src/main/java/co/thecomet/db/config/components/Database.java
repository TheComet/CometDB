package co.thecomet.db.config.components;

public class Database {

    private String host = "127.0.0.1";
    private int port = 27017;
    private String username = "admin";
    private String password = "";
    private String database = "network";

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getDatabase() {
        return database;
    }
}
