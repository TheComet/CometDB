package co.thecomet.db.mongodb;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import org.mongodb.morphia.Datastore;

import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.logging.Logger;

public class MongoResource {
    protected MongoClient mongoClient;
    protected DatabaseObject databaseObject;

    public MongoResource(DatabaseObject databaseObject, Logger logger) {
        this.databaseObject = databaseObject;
        this.mongoClient = initMongoClient(logger);
    }

    private MongoClient initMongoClient(Logger logger) {
        try {
            if (databaseObject.getUsername() != null && databaseObject.getUsername().isEmpty() == false
                    && databaseObject.getPassword() != null && databaseObject.getPassword().isEmpty() == false) {
                return withSecurity();
            } else {
                return withoutSecurity();
            }
        } catch (UnknownHostException e) {
            logger.warning(e.getMessage());
        }

        return null;
    }

    public MongoClient withSecurity() throws UnknownHostException {
        if (databaseObject.getDatabase() == null) {
            throw new UnknownHostException("Database field was not initialized.");
        } else {
            if (databaseObject.getDatabase().trim().equals("")) {
                throw new UnknownHostException("No database was specified");
            } else {
                return secure();
            }
        }
    }

    public MongoClient withoutSecurity() throws UnknownHostException {
        if (databaseObject.getDatabase() == null) {
            throw new UnknownHostException("Database field was not initialized.");
        } else {
            if (databaseObject.getDatabase().trim().equals("")) {
                throw new UnknownHostException("No database was specified");
            } else {
                return unsecured();
            }
        }
    }

    public MongoClient secure() throws UnknownHostException {
        MongoCredential credential = MongoCredential.createMongoCRCredential(
                databaseObject.getUsername(),
                databaseObject.getDatabase(),
                databaseObject.getPassword().toCharArray()
        );

        return new MongoClient(new ServerAddress(
                databaseObject.getHost(),
                databaseObject.getPort()), Arrays.asList(credential));
    }

    public MongoClient unsecured() throws UnknownHostException {
        return new MongoClient(databaseObject.getHost(), databaseObject.getPort());
    }

    protected void authenticate(Datastore datastore) {
        if (!databaseObject.getUsername().isEmpty() &&
                !databaseObject.getPassword().isEmpty()) {
            datastore.getDB().authenticateCommand(databaseObject.getUsername(), databaseObject.getPassword().toCharArray());
        }
    }
}
