package co.thecomet.db.mongodb;

import co.thecomet.db.DBPlugin;
import co.thecomet.db.config.Settings;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import java.util.Set;

public class ResourceManager {
    private static ResourceManager instance;
    private DBPlugin plugin;
    private MongoResource resource;
    private Morphia morphia;

    public ResourceManager(DBPlugin plugin) {
        instance = this;
        this.plugin = plugin;

        if (resource == null) {
            resource = new MongoResource(getDatabaseObject(), plugin.getLogger());
        }

        morphia = new Morphia();
    }

    private DatabaseObject getDatabaseObject() {
        Settings settings = plugin.getSettings();

        DatabaseObject databaseObject = new DatabaseObject(settings.getDatabase().getHost(), settings.getDatabase().getPort());

        if (settings.getDatabase().getUsername().equals("") == false && settings.getDatabase().getPassword().equals("") == false) {
            databaseObject.withUsername(settings.getDatabase().getUsername());
            databaseObject.withPassword(settings.getDatabase().getPassword());
        }

        if (settings.getDatabase().getDatabase().equals("") == false) {
            databaseObject.withDatabase(settings.getDatabase().getDatabase());
        }

        return databaseObject;
    }

    public Datastore getDatastore(Set<Class> databaseClasses) {
        Datastore datastore;

        datastore = morphia.map(databaseClasses).createDatastore(resource.mongoClient, resource.databaseObject.getDatabase());
        resource.authenticate(datastore);

        return datastore;
    }

    public Datastore getDatastore(String pkg) {
        Datastore datastore;

        datastore = morphia.mapPackage(pkg, true).createDatastore(resource.mongoClient, resource.databaseObject.getDatabase());
        resource.authenticate(datastore);

        return datastore;
    }

    public Datastore getDatastore() {
        Datastore datastore = morphia.createDatastore(resource.mongoClient, resource.databaseObject.getDatabase());
        resource.authenticate(datastore);

        return datastore;
    }

    public Morphia getMorphia() {
        return morphia;
    }

    public static ResourceManager getInstance() {
        return instance;
    }
}
