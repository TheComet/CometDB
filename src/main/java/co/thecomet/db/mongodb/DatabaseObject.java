package co.thecomet.db.mongodb;

public class DatabaseObject {
    private static final String DEFAULT_DATABASE = "minecraft";

    private String host;
    private int port;
    private String username;
    private String password;
    private String database;

    public DatabaseObject(String host, int port) {
        this(host, port, DEFAULT_DATABASE);
    }

    public DatabaseObject(String host, int port, String database) {
        this(host, port, "", "", database);
    }

    public DatabaseObject(String host, int port, String username, String password) {
        this(host, port, username, password, DEFAULT_DATABASE);
    }

    public DatabaseObject(String host, int port, String username, String password, String database) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
        this.database = database;
    }

    public DatabaseObject withUsername(String username) {
        this.username = username;

        return this;
    }

    public DatabaseObject withPassword(String password) {
        this.password = password;

        return this;
    }

    public DatabaseObject withDatabase(String database) {
        this.database = database;

        return this;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getDatabase() {
        return database;
    }
}
