package co.thecomet.db;

import co.thecomet.db.mongodb.ResourceManager;
import org.mongodb.morphia.dao.BasicDAO;

public class DB {
    private static DBPlugin plugin;

    public static void setPlugin(DBPlugin p) {
        plugin = p;
    }

    public static DBPlugin getPlugin() {
        return plugin;
    }

    public static ResourceManager getResourceManager() {
        return plugin.getMongoController().getResourceManager();
    }

    public static void registerDAO(BasicDAO dao, Class<?> entity) {
        plugin.getMongoController().register(dao, entity);
    }
}
