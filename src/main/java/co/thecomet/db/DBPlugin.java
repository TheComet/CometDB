package co.thecomet.db;

import co.thecomet.db.config.Settings;
import co.thecomet.db.mongodb.MongoController;

import java.util.logging.Logger;

public interface DBPlugin {
    public Settings getSettings();

    public Logger getLogger();

    public MongoController getMongoController();
}
