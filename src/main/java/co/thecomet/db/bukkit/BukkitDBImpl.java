package co.thecomet.db.bukkit;

import co.thecomet.db.DB;
import co.thecomet.db.DBPlugin;
import co.thecomet.db.config.Settings;
import co.thecomet.db.mongodb.MongoController;
import co.thecomet.common.config.JsonConfig;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class BukkitDBImpl extends JavaPlugin implements DBPlugin {

    private Settings settings = null;
    private MongoController mongoController;

    public void onEnable() {
        DB.setPlugin(this);

        if (!getDataFolder().exists()) {
            getLogger().info("Config folder not found! Creating...");
            getDataFolder().mkdir();
        }

        settings = JsonConfig.load(new File(getDataFolder(), "settings.json"), Settings.class);
        mongoController = new MongoController(this);
    }

    public Settings getSettings() {
        return settings;
    }

    public MongoController getMongoController() {
        return mongoController;
    }
}
